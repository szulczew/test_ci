# Test CI

## Into
Simple test of CI pipeline.

### Files:
```
.gitlab-ci.yml - CI configuration file.
my_script.py - Python script containg function to sum two ints.
test_my_script.py - test to be picked up by Pytest module.
```

### GitLab CI configuration file:
```
cat .gitlab-ci.yml

image: python:3.5                               # Dockerized image where test will be run.

before_script:                                  # Prepare environment
  - python -V                                   # Print out python version for debugging
  - pip install -r requirements.txt             # Install requirements

test:
  script:
  - pytest                                      # Execute all test files test_*.py
 ```

## CI pipeline runs with every commit.

When it pass:
https://gitlab.com/szulczew/test_ci/-/jobs/100865564

When CI fail:
https://gitlab.com/szulczew/test_ci/-/jobs/100856068

Review commit history for more information.
